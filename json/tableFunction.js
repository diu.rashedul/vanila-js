function loadData() {
    const url = "https://api.github.com/users";
    fetch(url)
        .then((resp) => resp.text())
        .then(function (data) {
            let countries = eval(data)
            Rashed.htmlTable.display(countries, '#root');
        })
}


window.addEventListener('load', function () {
    document.getElementById('btnLoadData').addEventListener('click', loadData)

});