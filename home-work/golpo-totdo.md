# Objective : displaying multiple todos from a todos json (API).

0 collecting comments data
           
3 preparing multiple todos

    1 preparing todo item
    
    2 preparing single todo
                 
# 0 collecting todos data:
### step 0A: prepare ajax call to fecth data 
### step 0B: collect the data


# 1 preparing todo item:
### step 1A: creating  container for todo item. e.g: using span node and input type
### step 1B: creating TXT node and checkbox
### step 1C: inserting txt node and checkbox into container node.

# 2 preparing single todo: 
### step 2A: creating container. e.g using list item (li)
### step 2B: insert span node and input node (1)  into container

# 3 preparing multiple todos: 
### step 3A: creating container. e.g using unorder list (ul)
### step 3B: insert list item(li)/single todo (3) into container

# 4 display the todos:
### step 4A: target the location
### step 4B: insert todos into targeted HTML
