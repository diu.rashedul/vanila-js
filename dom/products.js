// 'use strict'


// const data = (function (){

//     const products = [
//         {
//             id:1,
//             type:"Laptop",
//             brand:"HP",
//             price: 100000
//         },
//         {
//             id:2,
//             type:"Mouse",
//             brand:"A4",
//             price: 400
//         },
//         {
//             id:3,
//             type:"Keyboard",
//             brand:"Logitech",
//             price: 300
//         }
//     ]

//     return products;
// })(); // IIFE : Always one time execute

// function xyz(arg1){

// }

// let table = document.createElement('table'); // Virtual DOM

// for(let product of data){

//     let itemIdText = document.createTextNode(product.id);
//     let itemIdTd = document.createElement('td');
//     itemIdTd.appendChild(itemIdText);

//     let itemTypeText = document.createTextNode(product.type);
//     let itemTypeTd = document.createElement('td');
//     itemTypeTd.appendChild(itemTypeText);

//     let itemBrandText = document.createTextNode(product.brand);
//     let itemBrandTd = document.createElement('td');
//     itemBrandTd.appendChild(itemBrandText);

//     let itemPriceText = document.createTextNode(product.price);
//     let itemPriceTd = document.createElement('td');
//     itemPriceTd.appendChild(itemPriceText);


//     let tr = document.createElement('tr');  // find the reason why
//     tr.appendChild(itemIdTd);
//     tr.appendChild(itemTypeTd);
//     tr.appendChild(itemBrandTd);
//     tr.appendChild(itemPriceTd);


//     table.appendChild(tr);

// }

// console.log(table);
// let container = document.querySelector("#root")
// container.appendChild(table);


'use strict'
const data = (function () {
    const products = [{
            id: 1,
            type: "Laptop",
            brand: "HP",
            price: 100000,
            xyz: 'abc',
            xyz2: 'abc'

        },
        {
            id: 2,
            type: "Mouse",
            brand: "A4",
            price: 400,
            xyz: 'abc',
            xyz2: 'abc'

        },
        {
            id: 3,
            type: "Keyboard",
            brand: "Logitech",
            price: 300,
            xyz: 'abc',
            xyz2: 'abc'

        },
        {
            id: 4,
            type: "CPU",
            brand: "Transcend",
            price: 300000,
            xyz: 'abc',
            xyz2: 'abc',

        },

        {
            id: 5,
            type: "CPU2",
            brand: "Transcend2",
            price: 3000002,
            xyz: 'abc2',
            xyz2: 'abc'

        }
    ]

    return products;
})(); // IIFE : Always one time execute

function createTableHead(headText) {
    let headContent = document.createTextNode(headText);
    let head = document.createElement('TH');
    head.appendChild(headContent);

    return head;
}

function createTableCell(cellText) {
    let textNode = document.createTextNode(cellText);
    let container = document.createElement('TD');
    container.appendChild(textNode);

    return container;
}
// preparing the table for the data
// table
let table = document.createElement('table'); // Virtual DOM
//thead
let tr = document.createElement('tr');
for (let property in data[0]) {
    tr.appendChild(createTableHead(property.toUpperCase()));
}
table.appendChild(tr);
// table rows
for (let product of data) {
    let td = '';
    let tr = document.createElement('tr'); // find the reason why

    for (let property in product) {
        td = createTableCell(product[property])
        tr.appendChild(td)

    }
    table.appendChild(tr);
}
let container = document.querySelector("#root")
container.appendChild(table);