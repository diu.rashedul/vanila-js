let txt = "To Work with javascript built in objects"
let index = 0
let timer

function teletype() {
    let char = txt.charAt(index)
    window.document.getElementById('display').innerHTML += char
    index++

    timer = setTimeout(teletype, 500)
    

}

function stopTeletype() {
    clearTimeout(timer)
}

let btnStart = document.getElementById('btnStart')
btnStart.addEventListener('click', teletype)

let btnStop = document.getElementById('btnStop')
btnStop.addEventListener('click', stopTeletype)


//let stringLength = txt.length
//console.log(stringLength)
//console.log(txt.indexOf('k'))
//console.log(txt.charAt(6))
//console.log(txt.charCodeAt('k'))