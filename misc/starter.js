/*
1. Basic language : variables etc. let / const, function, string
2. BOM: alert
3. DOM: getElementById
4. Event: attachEventListener
5. AJAX :
6. JSON :
*/
// w3schools | MDN
//Basic
// let / const

function drive(){
    confirm("Are you driving?");
}

let drive2 = function(){

}

// declare a function
function addNumbers(number1, number2){ // provide inputs
    let result = number1 + number2;
    return result; // output
}

// declare a function expression
let addNumbers2 = function (number1, number2){ // provide inputs
    let result = number1 + number2;
    return result; // output
}

console.log(addNumbers2(2,3));
console.log(addNumbers2(4,5));

let aString = 'Hello how are you?';
let anotherString = "I am fine";
let aSampleTemplateLiteral = `I am fine`;

// Array Literal

let students = []; // declare an empty array
let teachers = ['t1','t2']; // declare array with values
console.log(teachers[0]); // access an array
console.log(teachers[1]);
teachers[2] = 't3'; // assign new value to an array
console.log(teachers[2]);

// Object Literal

let cat = {}; //declare an empty object
let car = {
    
    wheel : 4,
    seat  : 6,
    color : 'red',
    hasFuel: true,

    drive : function(){
        alert('I am driving');
    },

    openDoor: function(){
        alert('Opening the door');
    }


}; //declare an object with some properties
// and some activities


//BOM
//alert, prompt, confirm

let result = confirm("Are you fine?");
console.log(result);

//DOM
// document.getElementById() | document.querySelector('#id')

// VIRTUAL DOM
let txt = document.createTextNode("Hello! Virtual DOM.");
let div = document.createElement('DIV');
div.appendChild(txt);
div.setAttribute('style','background-color:yellow');

let body = document.getElementById('formContainer');
body.appendChild(div);


// EVENT: click/mouseover
body.addEventListener('click', drive);


// AJAX
let url = null;
fetch(url) // Call the fetch function passing the url of the API as a parameter
.then(function() {
    // Your code for handling the data you get from the API
})
.catch(function() {
    // This is where you run code if the server returns any errors
});

// JSON : Data Format
let countries = ['bangladesh', 'usa', 'canada'];

let bangladesh = {
                    name:'Bangladesh',code: 'BD',language: 'Bangla',city: 'Dhaka'
                };
 
let countries2 = [ { name:'Bangladesh',code: 'BD',language: 'Bangla',city: 'Dhaka'}, { name:'USA',code: 'US',language: 'English',city: 'Washington'}, {}];

console.log(countries);
console.log(bangladesh);
console.log(countries2);

for(country of countries){ 
    console.log(country);
}

for(country in countries){ 
    console.log(country);
}

for(country of countries2){
    console.log(country);
}

for(country in countries2){
    console.log(country);
}